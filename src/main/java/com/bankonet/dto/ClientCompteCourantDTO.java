package com.bankonet.dto;

public class ClientCompteCourantDTO extends ClientCompteDTO {

	private double montantDecouvertAutorise;

	public double getMontantDecouvertAutorise() {
		return montantDecouvertAutorise;
	}

	public void setMontantDecouvertAutorise(double montantDecouvertAutorise) {
		this.montantDecouvertAutorise = montantDecouvertAutorise;
	}
	
	
}
