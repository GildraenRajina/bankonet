package com.bankonet.dto;

import java.util.List;

public class ClientDTO {

	private int id;
	private String nom;
	private String prenom;
	private List<ClientCompteDTO> comptes;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public List<ClientCompteDTO> getComptes() {
		return comptes;
	}
	public void addCompte(ClientCompteDTO compte) {
		this.comptes.add(compte);
	}
	
	

}
