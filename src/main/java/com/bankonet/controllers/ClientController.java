package com.bankonet.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bankonet.dto.ClientDTO;
import com.bankonet.entities.Client;
import com.bankonet.exceptions.ClientNotFoundException;
import com.bankonet.services.IClientService;

@RestController
@RequestMapping("/api/client")
public class ClientController {
	
	@Autowired
	private IClientService clientService;
	
	@GetMapping("/{id}")
	public ClientDTO getClient(@PathVariable int id) {
		try {
			return clientService.getClient(id);
		} catch (ClientNotFoundException e) {
			System.out.println("log");
			return null;
		}
	}
	
	@GetMapping("/all")
	public List<Client> getAllClients() {
		return clientService.getAllClients();
	}
	
	@PostMapping("/new")
	public ClientDTO createClient(@RequestBody ClientDTO client) {
		return clientService.createClient(client);
	}
}
