package com.bankonet.controllers;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
@SpringBootApplication 
public class HelloWorldController {

	@RequestMapping("/")
	public String home() {
		return "Hello World!";
	}

}