package com.bankonet.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bankonet.dto.CompteCourantDTO;
import com.bankonet.entities.CompteCourant;
import com.bankonet.exceptions.CompteCourantNotFoundException;
import com.bankonet.services.CompteCourantService;
import com.bankonet.services.ICompteCourantService;

@RestController
@RequestMapping("/api/compte")
public class CompteController {
	
	@Autowired
	private ICompteCourantService compteService;
	
	@GetMapping("/courant/{id}")
	public CompteCourantDTO getCompteCourant(@PathVariable int id) {
		try {
			return compteService.getCompteCourant(id);
		} catch (CompteCourantNotFoundException e) {
			System.out.println("log");
			return null;
		}
	}
	
	@GetMapping("/courant/all")
	public List<CompteCourant> getAllCompteCourant() {
		return compteService.getAllCompteCourants();
	}
	
	@PostMapping("/new")
	public CompteCourantDTO createCompte(@RequestBody CompteCourantDTO compteCourantDTO) {
		return compteService.createCompteCourant(compteCourantDTO);
	}
}
