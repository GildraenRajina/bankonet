package com.bankonet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bankonet.entities.Compte;

@Repository
public interface CompteRepository  extends JpaRepository<Compte, Integer> {

	Compte findOneById(Integer id);
}

