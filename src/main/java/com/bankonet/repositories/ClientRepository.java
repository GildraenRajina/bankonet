package com.bankonet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bankonet.entities.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {

		Client findOneById(Integer id);
}
