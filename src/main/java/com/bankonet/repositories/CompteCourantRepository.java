package com.bankonet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bankonet.entities.Compte;
import com.bankonet.entities.CompteCourant;

@Repository
public interface CompteCourantRepository extends JpaRepository<CompteCourant, Integer> {

	CompteCourant findOneById(Integer id);

}
