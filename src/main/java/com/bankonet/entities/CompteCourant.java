package com.bankonet.entities;

import javax.persistence.Entity;


@Entity
public class CompteCourant extends Compte {

	private double montantDecouvertAutorise;

	public double getMontantDecouvertAutorise() {
		return montantDecouvertAutorise;
	}

	public void setMontantDecouvertAutorise(double montantDecouvertAutorise) {
		this.montantDecouvertAutorise = montantDecouvertAutorise;
	}
	
	
}
