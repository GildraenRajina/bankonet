package com.bankonet.mappers;

import org.mapstruct.Mapper;

import com.bankonet.dto.ClientCompteCourantDTO;
import com.bankonet.dto.CompteCourantDTO;
import com.bankonet.entities.CompteCourant;


@Mapper( componentModel = "spring",
uses=ClientMapper.class
)
public interface CompteCourantMapper {

	public CompteCourantDTO CompteCourantToCompteCourantDTO(CompteCourant compteCourant);
	
	public ClientCompteCourantDTO CompteCourantToClientCompteCourantDTO(CompteCourant compteCourant);

	public CompteCourant CompteCourantDTOToCompteCourant(CompteCourantDTO compteCourantDTO);
	
	public CompteCourant ClientCompteCourantDTOToCompteCourant(ClientCompteCourantDTO compteCourantDTO);
}
