package com.bankonet.mappers;

import org.mapstruct.Mapper;

import com.bankonet.dto.ClientDTO;
import com.bankonet.entities.Client;


@Mapper( componentModel = "spring",
uses=CompteMapper.class
)
public interface ClientMapper {

	public ClientDTO clientToClientDto(Client client);

	public Client clientDtoToClient(ClientDTO clientDto);
}
