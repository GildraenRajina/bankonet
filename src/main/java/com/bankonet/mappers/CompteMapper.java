package com.bankonet.mappers;

import org.mapstruct.Mapper;

import com.bankonet.dto.ClientCompteCourantDTO;
import com.bankonet.dto.ClientCompteDTO;
import com.bankonet.entities.Compte;
import com.bankonet.entities.CompteCourant;

@Mapper( componentModel = "spring",
uses=ClientMapper.class
)
public interface CompteMapper {

	//public ClientCompteDTO compteToClientCompteDto(Compte compte);
	
	public ClientCompteCourantDTO compteCourantToClientCompteCourantDto(CompteCourant compte);
	
	

	//public Compte clientCompteDtoToCompte(ClientCompteDTO clientDto);
	
	public CompteCourant clientCompteCourantDtoToCompteCourant(ClientCompteCourantDTO clientDto);
}
