package com.bankonet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankonetApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(BankonetApplication.class, args);
	}
}
