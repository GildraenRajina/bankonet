package com.bankonet.services;

import java.util.List;

import com.bankonet.dto.CompteCourantDTO;
import com.bankonet.exceptions.CompteCourantNotFoundException;

public interface ICompteService {
	
	public CompteCourantDTO getCompteCourant(Integer id) throws CompteCourantNotFoundException;
	
	public CompteCourantDTO createClient(CompteCourantDTO clientDto);
	
	public List<CompteCourantDTO> getAllClients();
}
