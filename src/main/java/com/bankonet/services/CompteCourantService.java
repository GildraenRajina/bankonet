package com.bankonet.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankonet.dto.CompteCourantDTO;
import com.bankonet.entities.Compte;
import com.bankonet.entities.CompteCourant;
import com.bankonet.exceptions.CompteCourantNotFoundException;
import com.bankonet.mappers.CompteCourantMapper;
import com.bankonet.repositories.CompteCourantRepository;

@Service
public class CompteCourantService implements ICompteCourantService {

	@Autowired
	private CompteCourantRepository compteCourantRepository;

	@Autowired
	private CompteCourantMapper compteCourantMapper;

	public CompteCourantDTO getCompteCourant(Integer id) throws CompteCourantNotFoundException {
		CompteCourant compteCourant = compteCourantRepository.findOneById(id);

		CompteCourantDTO compteCourantDto = compteCourantMapper.CompteCourantToCompteCourantDTO(compteCourant);

		if (compteCourantDto == null) {
			throw new CompteCourantNotFoundException("Le compteCourant avec l'id " + id + " n'éxiste pas ");
		}

		return compteCourantDto;
	}

	@Transactional
	public CompteCourantDTO createCompteCourant(CompteCourantDTO compteCourantDto) {
		
		//ToDO
//		CompteCourant compteCourant = new CompteCourant();
//		compteCourant.setIntitule(compteCourantDto.getIntitule());
//		compteCourant.set(compteCourantDto.getPrenom());
//		compteCourant = compteCourantRepository.saveAndFlush(compteCourant);
//
//		compteCourantDto = compteCourantMapper.CompteCourantToCompteCourantDTO(compteCourant);
		return compteCourantDto;
	}

	public List<CompteCourant> getAllCompteCourants() {
		return compteCourantRepository.findAll();
	}







}
