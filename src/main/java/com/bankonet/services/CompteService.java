package com.bankonet.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankonet.entities.Compte;
import com.bankonet.exceptions.CompteNotFoundException;
import com.bankonet.repositories.CompteRepository;

@Service
@Transactional
public class CompteService {

	

	private CompteRepository compteRepository;

	public Compte getCompte(Integer id) throws CompteNotFoundException {
		Compte compte = compteRepository.findOneById(id);

		if (compte == null) {
			throw new CompteNotFoundException("Not found bro");
		}
		return compte;
	}

	@Transactional
	public Compte createCompte(Compte compte) {
		compte = compteRepository.saveAndFlush(compte);
		return compte;
	}

	public List<Compte> getAllCompte() {
		return compteRepository.findAll();
	}
}
