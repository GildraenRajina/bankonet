package com.bankonet.services;

import java.util.List;

import com.bankonet.dto.ClientDTO;
import com.bankonet.entities.Client;
import com.bankonet.exceptions.ClientNotFoundException;

public interface IClientService {
	
	public ClientDTO getClient(Integer id) throws ClientNotFoundException;
	
	public ClientDTO createClient(ClientDTO clientDto);
	
	public List<Client> getAllClients();
}
