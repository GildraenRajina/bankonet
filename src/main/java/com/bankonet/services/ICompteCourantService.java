package com.bankonet.services;

import java.util.List;

import com.bankonet.dto.CompteCourantDTO;
import com.bankonet.entities.CompteCourant;
import com.bankonet.exceptions.CompteCourantNotFoundException;

public interface ICompteCourantService {
	
	public CompteCourantDTO getCompteCourant(Integer id) throws CompteCourantNotFoundException;
	
	public CompteCourantDTO createCompteCourant(CompteCourantDTO clientDto);
	
	public List<CompteCourant> getAllCompteCourants();
}
