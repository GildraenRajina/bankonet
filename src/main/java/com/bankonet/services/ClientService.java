package com.bankonet.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankonet.dto.ClientDTO;
import com.bankonet.entities.Client;
import com.bankonet.exceptions.ClientNotFoundException;
import com.bankonet.mappers.ClientMapper;
import com.bankonet.repositories.ClientRepository;

@Service
public class ClientService implements IClientService {

	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private ClientMapper clientMapper;

	
	public ClientDTO getClient(Integer id) throws ClientNotFoundException {
		Client client = clientRepository.findOneById(id);
		
		
		ClientDTO clientDto = clientMapper.clientToClientDto(client);
		
		if (clientDto == null) {
			throw new ClientNotFoundException("Le client avec l'id " + id + " n'éxiste pas ");
		}		
		
		return clientDto;
	}
	

	@Transactional
	public ClientDTO createClient(ClientDTO clientDto) {
		Client client = new Client();
		client.setNom(clientDto.getNom());
		client.setPrenom(clientDto.getPrenom());
		client = clientRepository.saveAndFlush(client);
		
		clientDto = clientMapper.clientToClientDto(client);
		return clientDto;
	}

	public List<Client> getAllClients() {
		return clientRepository.findAll();
	}
	


}

