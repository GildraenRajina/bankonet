package com.bankonet.exceptions;

public class CompteNotFoundException extends Exception {
	
	public CompteNotFoundException(String errorMessage) {
		super(errorMessage);
	}
}
