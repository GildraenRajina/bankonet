package com.bankonet.exceptions;

public class CompteCourantNotFoundException extends Exception {
	
	public CompteCourantNotFoundException(String errorMessage) {
		super(errorMessage);
	}
}
