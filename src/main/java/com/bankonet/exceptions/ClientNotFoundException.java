package com.bankonet.exceptions;

public class ClientNotFoundException extends Exception {

	public ClientNotFoundException(String errorMessage) {
		super(errorMessage);
	}

}
